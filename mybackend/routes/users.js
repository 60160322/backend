const express = require('express')
const router = express.Router()
const usersController = require('../controller/UsersController')
const User = require('../models/User')
/* GET users listing. */
// eslint-disable-next-line no-unused-vars

router.get('/', usersController.getUsers)
router.get('/:id', usersController.getUsers)
router.post('/', usersController.addUser)
router.put('/', usersController.updateUser)
router.delete('/:id',usersController.deleteUser)

// eslint-disable-next-line no-unused-vars
router.get('/', async (req, res, next) => {

  // res.json(usersController.getUsers())
  //1
  //User.find({}).exec(function (err,  users) {
  // if (err) {
  // res.status(500).send()
  //  }
  // res.json(users)
  // })
  //2
  //User.find({}).then(function(users){
  //res.json(users)
  //}).catch(function (err){
  //res.status(500).send(err)
  //})
  //3
  try {
    const users = await User.find({})
    res.json(users)
  } catch (err) {
    res.status(500).send(err)
  }
})

// eslint-disable-next-line no-unused-vars
router.get('/:id', async (req, res, next) => {
  const { id } = req.params
  // res.json(usersController.getUser(id))
  //1
  //User.findById(id).then( function (user) {
  //res.json(user)
  //}).catch(function (err) {
  //res.status(500).send(err)
  //})

  //2
  try {
    const user = await User.findById(id)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }

})

// eslint-disable-next-line no-unused-vars
router.post('/', async (req, res, next) => {
  const payload = req.body
  /// User.create(payload).then(function (user) {
  //res.json(user)

  ///}).catch(function (err) {
  //res.status(500).send(err)
  //})
  //res.json(usersController.addUser(payload))
  console.log(payload)
  const user = new User(payload)
  try {
    await user.save()
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

// eslint-disable-next-line no-unused-vars
router.put('/', async (req, res, next) => {
  const payload = req.body
  //res.json(usersController.updateUser(payload))
  try {
    const user = await User.updateOne({ _id: payload.id }, payload)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }


})

// eslint-disable-next-line no-unused-vars
router.delete('/:id', async (req, res, next) => {
  const { id } = req.params
  //res.json(usersController.deleteUser(id))
  try {
    const user = await User.deleteOne({ _id: id })
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }





})

module.exports = router